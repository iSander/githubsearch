//
//  Constants.h
//  GitHubSearch
//
//  Created by Alex Sander on 08.01.2018.
//  Copyright © 2018 Alex Sander. All rights reserved.
//

#define SEARCH_REPOSITORIES_URL     @"https://api.github.com/search/repositories?q=%@"

#define CELL_IDENTIFIER             @"cellIdentifier"
#define CELL_XIB_NAME               @"RepositoryTableViewCell"
