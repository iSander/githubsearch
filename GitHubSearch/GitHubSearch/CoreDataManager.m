//
//  CoreDataManager.m
//  GitHubSearch
//
//  Created by Alex Sander on 08.01.2018.
//  Copyright © 2018 Alex Sander. All rights reserved.
//

#import "CoreDataManager.h"

@implementation CoreDataManager

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer
{
    @synchronized (self)
    {
        if (_persistentContainer == nil)
        {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"GitHubSearch"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error)
            {
                if (error != nil)
                {
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

+ (instancetype)shared
{
    static CoreDataManager *shared;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [CoreDataManager new];
        shared.context = shared.persistentContainer.viewContext;
        
    NSLog(@"CoreDataManager init");
    });
    return shared;
}

- (void)saveContext
{
    NSError *error = nil;
    if ([self.context hasChanges] && ![self.context save:&error])
    {
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

- (void)addData:(RepositoryData*)_repositoryData
{
    if([self isSameDataExist:_repositoryData])
    {
        NSLog(@"repo %@ alredy exist", _repositoryData.name);
        return;
    }
    
    Repository *rep = [[Repository alloc] initWithContext:self.context];
    rep.name = _repositoryData.name;
    rep.ownerImageUrl = _repositoryData.ownerImageUrl;
    rep.ownerName = _repositoryData.ownerName;
    rep.text = _repositoryData.text;
    rep.stars = _repositoryData.stars;
    rep.url = _repositoryData.url;
    
    [self saveContext];
}

- (void)deleteData:(Repository*)_repository
{
    [self.context deleteObject:_repository];
    [self saveContext];
}

- (NSArray<Repository*>*) fetchData
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Repository"];
    NSArray<Repository*> *array = [self.context executeFetchRequest:fetchRequest error:nil];
    
    return array;
}

- (BOOL) isSameDataExist:(RepositoryData*)_repositoryData
{
    NSArray<Repository*> *array = [self fetchData];
    
    for (Repository *r in array)
    {
        if([_repositoryData.name isEqualToString:r.name] && [_repositoryData.ownerName isEqualToString:r.ownerName])
            return YES;
    }
    
    return NO;
}

- (void)printData
{
    NSLog(@"printData");
    
    NSArray<Repository*> *array = [self fetchData];
    
    for (Repository *r in array)
    {
        NSLog(@"repo name: %@", r.name);
    }
}

@end
