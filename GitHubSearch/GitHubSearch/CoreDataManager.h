//
//  CoreDataManager.h
//  GitHubSearch
//
//  Created by Alex Sander on 08.01.2018.
//  Copyright © 2018 Alex Sander. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "RepositoryData.h"
#import "Repository+CoreDataClass.h"

@interface CoreDataManager : NSObject
{
    
}

@property (nonatomic) NSManagedObjectContext *context;
@property (readonly, strong) NSPersistentContainer *persistentContainer;

+ (instancetype)shared;

- (void)saveContext;
- (void)addData:(RepositoryData*)_repositoryData;
- (void)deleteData:(Repository*)_repository;
- (NSArray<Repository*>*)fetchData;
- (void)printData;

@end
