//
//  SearchViewController.h
//  GitHubSearch
//
//  Created by Alex Sander on 06.01.2018.
//  Copyright © 2018 Alex Sander. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RepositoryData.h"

@interface SearchViewController : UIViewController
{
    NSMutableArray<RepositoryData*> *searchResults;
}

@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UITableView *searchTableView;

@end
