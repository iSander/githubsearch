//
//  RepositoryData.h
//  GitHubSearch
//
//  Created by Alex Sander on 08.01.2018.
//  Copyright © 2018 Alex Sander. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RepositoryData : NSObject
{
    
}

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *ownerImageUrl;
@property (nonatomic, copy) NSString *ownerName;
@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSString *url;
@property (nonatomic) int64_t stars;

-(id) initWithDictionary:(NSDictionary*)_dictionary;

@end
