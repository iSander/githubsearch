//
//  RepositoryData.m
//  GitHubSearch
//
//  Created by Alex Sander on 08.01.2018.
//  Copyright © 2018 Alex Sander. All rights reserved.
//

#import "RepositoryData.h"

@implementation RepositoryData

-(id) initWithDictionary:(NSDictionary*)_dictionary
{
    self = [super init];
    
    _name = _dictionary[@"name"];
    _ownerImageUrl = _dictionary[@"owner"][@"avatar_url"];
    _ownerName = _dictionary[@"owner"][@"login"];
    _url = _dictionary[@"html_url"];
    _stars = [_dictionary[@"stargazers_count"] intValue];
    _text = _dictionary[@"description"];
    
    if ([_text isKindOfClass:[NSNull class]])
        _text = @"";

    return self;
}

@end
