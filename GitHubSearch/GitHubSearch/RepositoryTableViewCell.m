//
//  RepositoryTableViewCell.m
//  GitHubSearch
//
//  Created by Alex Sander on 08.01.2018.
//  Copyright © 2018 Alex Sander. All rights reserved.
//

#import "RepositoryTableViewCell.h"

@implementation RepositoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
