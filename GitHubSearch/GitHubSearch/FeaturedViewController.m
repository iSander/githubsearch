//
//  FeaturedViewController.m
//  GitHubSearch
//
//  Created by Alex Sander on 06.01.2018.
//  Copyright © 2018 Alex Sander. All rights reserved.
//

#import "FeaturedViewController.h"
#import "CoreDataManager.h"
#import "RepositoryTableViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "DetailedViewController.h"
#import "Constants.h"

@implementation FeaturedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [_featuredTableView registerNib:[UINib nibWithNibName:CELL_XIB_NAME bundle:nil] forCellReuseIdentifier:CELL_IDENTIFIER];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    savedRepositories = [NSMutableArray arrayWithArray:[[CoreDataManager shared] fetchData]];
    [_featuredTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Table View

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [savedRepositories count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RepositoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFIER];
    
    if (cell == nil)
    {
        cell = [[RepositoryTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CELL_IDENTIFIER];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    Repository *repoData = savedRepositories[indexPath.row];
    
    cell.nameLabel.text = repoData.name;
    cell.ownerNameLabel.text = repoData.ownerName;
    cell.descriptionLabel.text = repoData.text;
    cell.urlLabel.text = repoData.url;
    cell.starsLabel.text = [NSString stringWithFormat:@"★%lli", repoData.stars];
    cell.starsLabel.hidden = (repoData.stars == 0);
    [cell.ownerImageView setImageWithURL:[NSURL URLWithString:repoData.ownerImageUrl]];
    
    return cell;
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                        {
                                            [[CoreDataManager shared] deleteData:savedRepositories[indexPath.row]];
                                            [savedRepositories removeObjectAtIndex:indexPath.row];
                                            [_featuredTableView reloadData];
                                        }];
    deleteAction.backgroundColor = [UIColor redColor];
    
    return @[deleteAction];
}

- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    DetailedViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"DetailedViewController"];
    [self presentViewController:vc animated:YES completion:nil];
    
    Repository *repoData = savedRepositories[indexPath.row];
    
    vc.nameLabel.text = repoData.name;
    vc.ownerNameLabel.text = repoData.ownerName;
    vc.descriptionLabel.text = repoData.text;
    vc.urlLabel.text = repoData.url;
    vc.starsLabel.text = [NSString stringWithFormat:@"★%lli", repoData.stars];
    vc.starsLabel.hidden = (repoData.stars == 0);
    [vc.ownerImageView setImageWithURL:[NSURL URLWithString:repoData.ownerImageUrl]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
