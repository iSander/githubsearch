//
//  SearchViewController.m
//  GitHubSearch
//
//  Created by Alex Sander on 06.01.2018.
//  Copyright © 2018 Alex Sander. All rights reserved.
//

#import "SearchViewController.h"
#import "RepositoryTableViewCell.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "CoreDataManager.h"
#import "Constants.h"


@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [_searchTableView registerNib:[UINib nibWithNibName:CELL_XIB_NAME bundle:nil] forCellReuseIdentifier:CELL_IDENTIFIER];
    
    searchResults = [[NSMutableArray alloc] init];
    
    _searchTableView.estimatedRowHeight = 120;
    _searchTableView.rowHeight = UITableViewAutomaticDimension;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)searchAction:(id)sender
{
    if(_searchTextField.text != nil && ![_searchTextField.text isEqualToString:@""])
    {
        NSLog(@"search: %@", _searchTextField.text);
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        NSString *urlString = [NSString stringWithFormat:SEARCH_REPOSITORIES_URL, _searchTextField.text];
        [manager GET:[urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]
          parameters:nil
            progress:nil
             success:^(NSURLSessionTask *task, id responseObject)
         {
            if(responseObject && [responseObject isKindOfClass:[NSDictionary class]])
            {
                [self parseResponse:[responseObject objectForKey:@"items"]];
            }
        }
             failure:^(NSURLSessionTask *operation, NSError *error)
        {
            NSLog(@"Error: %@", error);
        }];

    }
}

-(void) parseResponse:(NSDictionary*)_dictionary
{
    [searchResults removeAllObjects];
    
    for (NSDictionary *dict in _dictionary)
    {
        RepositoryData *repoData = [[RepositoryData alloc] initWithDictionary:dict];
        [searchResults addObject:repoData];
    }
    
    [_searchTableView reloadData];
}

#pragma mark -
#pragma mark Table View

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [searchResults count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RepositoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFIER];
    
    if (cell == nil)
    {
        cell = [[RepositoryTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CELL_IDENTIFIER];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    RepositoryData *repoData = searchResults[indexPath.row];
    
    cell.nameLabel.text = repoData.name;
    cell.ownerNameLabel.text = repoData.ownerName;
    cell.descriptionLabel.text = repoData.text;
    cell.urlLabel.text = repoData.url;
    cell.starsLabel.text = [NSString stringWithFormat:@"★%lli", repoData.stars];
    cell.starsLabel.hidden = (repoData.stars == 0);
    [cell.ownerImageView setImageWithURL:[NSURL URLWithString:repoData.ownerImageUrl]];
    
    return cell;
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewRowAction *saveAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Save" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
    {
        [[CoreDataManager shared] addData:searchResults[indexPath.row]];
    }];
    saveAction.backgroundColor = [UIColor lightGrayColor];
    
    return @[saveAction];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
