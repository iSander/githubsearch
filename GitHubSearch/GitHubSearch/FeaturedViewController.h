//
//  FeaturedViewController.h
//  GitHubSearch
//
//  Created by Alex Sander on 06.01.2018.
//  Copyright © 2018 Alex Sander. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Repository+CoreDataClass.h"

@interface FeaturedViewController : UIViewController
{
    NSMutableArray<Repository*> *savedRepositories;
}

@property (weak, nonatomic) IBOutlet UITableView *featuredTableView;

@end
