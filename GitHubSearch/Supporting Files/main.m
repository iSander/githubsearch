//
//  main.m
//  GitHubSearch
//
//  Created by Alex Sander on 06.01.2018.
//  Copyright © 2018 Alex Sander. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
